<?php

// Creating the widget 
class opcstm_horizons_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
		// Base ID of your widget
		'opcstm_horizons_widget', 

		// Widget name will appear in UI
		__('New Horizons Widget', 'opcstm_horizons_widget'), 

		// Widget description
		array( 'description' => __( 'A widget to display the lastest from New Horizons. *Must be manually updated*', 'opcstm_horizons_widget' ), ) 
		);
	}

	public function widget( $args, $instance ) {

		echo $args['before_widget'];

		echo '<div class="custom-widget-image">';
		
		echo '<a href="' . get_the_permalink($instance['post_id']) . '">';
		echo get_the_post_thumbnail($instance['post_id'], 'front-page-bottom');
		echo '</a>';
		
		echo '<div class="custom-widget-image-bar">';
		echo '<a href="' . get_the_permalink($instance['post_id']) . '">';
		echo 'As Seen in NEW HORIZONS';
		echo '</a>';
		echo '<div class="home-widget-drop-menu-button">';
		echo '<a href="' . $instance['archive'] . '">Archive</a>';
		echo '</div>';
		echo '</div>';
		
		echo '</div>';
		

		echo '<a class="latest-issue-link" href="' . get_the_permalink($instance['post_id']) . '">';
		echo 'Latest Item: <span class="latest-issue-link-topic">' . $instance['latest_issue'] . '</span>';
		echo '</a>';
		
		echo $args['after_widget'];
	}
			
	// Widget Backend 
	public function form( $instance ) {
	
		if ( isset( $instance[ 'post_id' ] ) ) {
		$post_id = $instance[ 'post_id' ];
		} else { $post_id = null; }
		
		if ( isset( $instance[ 'archive' ] ) ) {
		$archive = $instance[ 'archive' ];
		} else { $archive = null; }
		
		if ( isset( $instance[ 'latest_issue' ] ) ) {
		$latest_issue = $instance[ 'latest_issue' ];
		} else { $latest_issue = null; }
		
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'post_id' ); ?>"><?php _e( 'New Horizons Post ID:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'post_id' ); ?>" name="<?php echo $this->get_field_name( 'post_id' ); ?>" type="text" value="<?php echo esc_attr( $post_id ); ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'archive' ); ?>"><?php _e( 'Archive Link:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'archive' ); ?>" name="<?php echo $this->get_field_name( 'archive' ); ?>" type="text" value="<?php echo esc_attr( $archive ); ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'latest_issue' ); ?>"><?php _e( 'Latest Issue:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'latest_issue' ); ?>" name="<?php echo $this->get_field_name( 'latest_issue' ); ?>" type="text" value="<?php echo esc_attr( $latest_issue ); ?>" />
		</p>
		
		
		<?php 
		
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
	
		$instance = array();
		$instance['post_id'] = ( ! empty( $new_instance['post_id'] ) ) ? strip_tags( $new_instance['post_id'] ) : '';
		$instance['archive'] = ( ! empty( $new_instance['archive'] ) ) ? strip_tags( $new_instance['archive'] ) : '';
		$instance['latest_issue'] = ( ! empty( $new_instance['latest_issue'] ) ) ? strip_tags( $new_instance['latest_issue'] ) : '';
		return $instance;
		
	}
	
} // Class wpb_widget ends here

// Register and load the widget
function opcstm_load_horizons_widget() {
	register_widget( 'opcstm_horizons_widget' );
}
add_action( 'widgets_init', 'opcstm_load_horizons_widget' );