jQuery(function($) {

	if(window.innerWidth < 1024) {

	$('body').addClass("has-responsive-menu");
	$("nav .genesis-nav-menu").addClass("responsive-menu").before('<div id="responsive-menu-icon"></div>');
	
	$('.title-area').before('<div class="responsive-menu-button responsive-menu-toggle"></div>');
	
	$('.responsive-menu-toggle').click(function(){
	
		$('.responsive-menu').slideToggle(200);

	});
	
		//Handle menu clicking
		$(".responsive-menu > .menu-item").click(function(event){
			if (event.target !== this)
			return;
				$(this).find(".sub-menu:first").slideToggle();
		});
		
	}
	
	$(window).resize(function() {

		if(window.innerWidth < 1024) {
		
			if (!($('body').hasClass('has-responsive-menu'))) {
				
				$('body').addClass("has-responsive-menu");
				$("nav .genesis-nav-menu").addClass("responsive-menu");
				
				$( '.responsive-menu' ).css('display', 'none');
				$( '.responsive-menu ul.sub-menu' ).css('display', 'none');
				
				$('.title-area').before('<div class="responsive-menu-button responsive-menu-toggle"></div>');
				
				//If menu toggle clicked
				$('.responsive-menu-toggle').unbind('click').click(function(){
				
					$('.responsive-menu').slideToggle(200);

				});
				
				//If dropdown toggle clicked
				$(".responsive-menu > .menu-item").unbind('click').click(function(event){
					if (event.target !== this)
					return;
						$(this).find(".sub-menu:first").slideToggle();
				});
					
			}

		} else {
		
			$( '.responsive-menu' ).css('display', 'block');
			$( '.responsive-menu ul.sub-menu' ).css( 'display', '' );
		
			$( 'body' ).removeClass( 'has-responsive-menu' );
			$( 'nav .genesis-nav-menu' ).removeClass( 'responsive-menu' );
			
			$( '.responsive-menu-toggle' ).unbind( 'click' );
			$( '.responsive-menu > .menu-item' ).unbind( 'click' );
			
			$( '.responsive-menu-toggle' ).remove();
		
		}

	});

	
	$('.hwdmb').click(function(e){
	
		e.preventDefault();
		$(e.target).parent().children('.home-widget-drop-menu').slideToggle(200);
	
	});
	
});