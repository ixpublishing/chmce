<?php

add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_sidebar_content_sidebar' );

//Replace frontpage loop with frontpage widget area
remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action ( 'genesis_loop', 'opcstm_frontpage' );

function opcstm_frontpage() {
	
	genesis_widget_area ( 'home-middle', array(
		'before' => '',
		'after' => '',
	) );

}

//Replace the primary sidebar with frontpage left sidebar
remove_action('genesis_sidebar','genesis_do_sidebar');
add_action('genesis_sidebar', 'opcstm_home_left_sidebar');

function opcstm_home_left_sidebar() {
	
	genesis_widget_area ( 'home-left', array(
		'before' => '<div>',
		'after' => '</div>',
	) );

}

//Replace the secondary sidebar with frontpage right sidebar
remove_action( 'genesis_sidebar_alt', 'genesis_do_sidebar_alt' );
add_action( 'genesis_sidebar_alt', 'opcstm_home_right_sidebar' );

function opcstm_home_right_sidebar() {
	
	genesis_widget_area ( 'home-right', array(
		'before' => '<div>',
		'after' => '</div>',
	) );

}

genesis();