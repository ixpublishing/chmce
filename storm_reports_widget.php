<?php

// Creating the widget 
class opcstm_storm_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
		// Base ID of your widget
		'opcstm_storm_widget', 

		// Widget name will appear in UI
		__('S.T.O.R.M. Reports Widget', 'opcstm_storm_widget'), 

		// Widget description
		array( 'description' => __( 'A widget to display the lastest S.T.O.R.M. Report. *Must be manually updated*', 'opcstm_storm_widget' ), ) 
		);
	}

	public function widget( $args, $instance ) {

		echo $args['before_widget'];

		echo '<div class="custom-widget-image">';
		
		echo '<a href="' . get_the_permalink($instance['post_id']) . '">';
		echo get_the_post_thumbnail($instance['post_id'], 'front-page-bottom');
		echo '</a>';
		
		echo '<div class="custom-widget-image-bar">';
		echo '<a href="' . get_the_permalink($instance['post_id']) . '">';
		echo 'S.T.O.R.M. Report';
		echo '</a>';
		echo '<div class="home-widget-drop-menu-button">';
		echo '<a class="hwdmb" href="#">Download</a>';
		echo '<ul class="home-widget-drop-menu">';
		echo '<li><a href="' . $instance['full_report'] . '">Full Report</a></li>';
		echo '<li><a href="' . $instance['bulletin_insert'] . '">Printable Bulletin Insert</a></li>';
		echo '<li><a href="' . $instance['poster'] . '">Printable Poster</a></li>';
		echo '</ul>';
		echo '</div>';
		echo '</div>';
		
		echo '</div>';
		

		echo '<a class="latest-issue-link" href="' . $instance['full_report'] . '">';
		echo 'Latest Issue: <span class="latest-issue-link-topic">' . $instance['latest_issue'] . '</span>';
		echo '</a>';
		
		echo $args['after_widget'];
	}
			
	// Widget Backend 
	public function form( $instance ) {
	
		if ( isset( $instance[ 'post_id' ] ) ) {
		$post_id = $instance[ 'post_id' ];
		} else { $post_id = null; }
		
		if ( isset( $instance[ 'full_report' ] ) ) {
		$full_report = $instance[ 'full_report' ];
		} else { $full_report = null; }
		
		if ( isset( $instance[ 'bulletin_insert' ] ) ) {
		$bulletin_insert = $instance[ 'bulletin_insert' ];
		} else { $bulletin_insert = null; }
		
		if ( isset( $instance[ 'poster' ] ) ) {
		$poster = $instance[ 'poster' ];
		} else { $poster = null; }
		
				if ( isset( $instance[ 'latest_issue' ] ) ) {
		$latest_issue = $instance[ 'latest_issue' ];
		} else { $latest_issue = null; }
		
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'post_id' ); ?>"><?php _e( 'S.T.O.R.M. Reports Post ID:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'post_id' ); ?>" name="<?php echo $this->get_field_name( 'post_id' ); ?>" type="text" value="<?php echo esc_attr( $post_id ); ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'full_report' ); ?>"><?php _e( 'Full Report Link:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'full_report' ); ?>" name="<?php echo $this->get_field_name( 'full_report' ); ?>" type="text" value="<?php echo esc_attr( $full_report ); ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'bulletin_insert' ); ?>"><?php _e( 'Bulletin Insert Link:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'bulletin_insert' ); ?>" name="<?php echo $this->get_field_name( 'bulletin_insert' ); ?>" type="text" value="<?php echo esc_attr( $bulletin_insert ); ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'poster' ); ?>"><?php _e( 'Poster Link:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'poster' ); ?>" name="<?php echo $this->get_field_name( 'poster' ); ?>" type="text" value="<?php echo esc_attr( $poster ); ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'latest_issue' ); ?>"><?php _e( 'Latest Issue:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'latest_issue' ); ?>" name="<?php echo $this->get_field_name( 'latest_issue' ); ?>" type="text" value="<?php echo esc_attr( $latest_issue ); ?>" />
		</p>
		
		
		<?php 
		
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
	
		$instance = array();
		$instance['post_id'] = ( ! empty( $new_instance['post_id'] ) ) ? strip_tags( $new_instance['post_id'] ) : '';
		$instance['full_report'] = ( ! empty( $new_instance['full_report'] ) ) ? strip_tags( $new_instance['full_report'] ) : '';
		$instance['bulletin_insert'] = ( ! empty( $new_instance['bulletin_insert'] ) ) ? strip_tags( $new_instance['bulletin_insert'] ) : '';
		$instance['poster'] = ( ! empty( $new_instance['poster'] ) ) ? strip_tags( $new_instance['poster'] ) : '';
		$instance['latest_issue'] = ( ! empty( $new_instance['latest_issue'] ) ) ? strip_tags( $new_instance['latest_issue'] ) : '';
		return $instance;
		
	}
	
} // Class wpb_widget ends here

// Register and load the widget
function opcstm_load_storm_widget() {
	register_widget( 'opcstm_storm_widget' );
}
add_action( 'widgets_init', 'opcstm_load_storm_widget' );