<?php

add_filter( 'genesis_initial_layouts', 'opcstm_correct_layout_img', 10, 2);

function opcstm_correct_layout_img( $layouts, $url ) {	

	$layouts['sidebar-content-sidebar']['img'] = get_stylesheet_directory_uri() . '/images/scs.gif';
	$layouts['content-sidebar']['img'] = get_stylesheet_directory_uri() . '/images/cs.gif';
	$layouts['sidebar-content']['img'] = get_stylesheet_directory_uri() . '/images/sc.gif';

	return $layouts;
}

//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Enqueue Google Fonts
add_action( 'wp_enqueue_scripts', 'genesis_sample_google_fonts' );

function genesis_sample_google_fonts() {

	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Open+Sans:400,700|Cantata+One', array() );
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'opcstm_responsive_menu',  get_stylesheet_directory_uri() . '/js/responsive_menu.js', array('jquery') );
	wp_enqueue_style( 'dashicons' );

}



//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Unregister unneeded layouts
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );

//* Output header <img> in title area
add_filter( 'genesis_seo_title', 'add_image_to_title', 10, 3);

function add_image_to_title($title, $inside, $wrap) {

	$site_url  = get_bloginfo('url');
	$theme_url = get_stylesheet_directory_uri();
    return sprintf( '<div class="site-logo"><a href="%4$s"><img src="%3$s/images/logo.svg" alt="OPC Short Term Missions Logo" /></a></div><div class="site-title">%2$s</div>', $wrap, $inside, $theme_url, $site_url );

}

add_image_size( 'front-page-bottom', 372, 220, true );

remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_header', 'genesis_do_nav', 11 );

//* Register widget areas
genesis_register_sidebar( array(
	'id'				=> 'home-middle',
	'name'			=> __( 'Home Middle', 'opcstm_theme' ),
	'description'	=> __( 'This is middle of the homepage.', 'opcstm_theme' )
) );

genesis_register_sidebar( array(
	'id'				=> 'home-left',
	'name'			=> __( 'Home Left', 'opcstm_theme' ),
	'description'	=> __( 'This is left side of the homepage.', 'opcstm_theme' )
) );

genesis_register_sidebar( array(
	'id'				=> 'home-right',
	'name'			=> __( 'Home Right', 'opcstm_theme' ),
	'description'	=> __( 'This is right side of the homepage.', 'opcstm_theme' )
) );

genesis_register_sidebar( array(
	'id'				=> 'footer-widgets',
	'name'			=> __( 'Footer Widgets', 'opcstm_theme' ),
	'description'	=> __( 'This is the footer widget area. You can place up to 3 widgets here.', 'opcstm_theme' )
) );

remove_action( 'genesis_header', 'genesis_do_header' );
add_action( 'genesis_header', 'opcstm_do_header', 10 );

/**
 * Echo the default header, including the #title-area div, along with #title and #description, as well as the .widget-area.
 *
 * Does the `genesis_site_title`, `genesis_site_description` and `genesis_header_right` actions.
 *
 * @since 1.0.2
 *
 * @global $wp_registered_sidebars Holds all of the registered sidebars.
 *
 * @uses genesis_markup() Apply contextual markup.
 */

function opcstm_do_header() {

	global $wp_registered_sidebars;

	if ( ( isset( $wp_registered_sidebars['header-right'] ) && is_active_sidebar( 'header-right' ) ) || has_action( 'genesis_header_right' ) ) {

		genesis_markup( array(
			'html5'   => '<aside %s>',
			'xhtml'   => '<div class="widget-area header-widget-area">',
			'context' => 'header-widget-area',
		) );

			do_action( 'genesis_header_right' );
			add_filter( 'wp_nav_menu_args', 'genesis_header_menu_args' );
			add_filter( 'wp_nav_menu', 'genesis_header_menu_wrap' );
			dynamic_sidebar( 'header-right' );
			remove_filter( 'wp_nav_menu_args', 'genesis_header_menu_args' );
			remove_filter( 'wp_nav_menu', 'genesis_header_menu_wrap' );

		genesis_markup( array(
			'html5' => '</aside>',
			'xhtml' => '</div>',
		) );
	}

	genesis_markup( array(
		'html5'   => '<div %s>',
		'xhtml'   => '<div id="title-area">',
		'context' => 'title-area',
	) );

	do_action( 'genesis_site_title' );
	do_action( 'genesis_site_description' );

	echo '</div>';

}



remove_action( 'genesis_footer', 'genesis_do_footer' );
add_action( 'genesis_footer', 'opcstm_do_footer' );

function opcstm_do_footer() { ?>

<?php

	genesis_widget_area ( 'footer-widgets', array(
		'before' => '<div class="footer-widgets">',
		'after' => '</div>',
	) );

?>

<div class="footer-flag"></div>

<?php }

//* Filter breadcrumbs
add_filter( 'genesis_breadcrumb_args', 'fpchurch_prefix_breadcrumb' );
function fpchurch_prefix_breadcrumb( $args ) {
  $args['labels']['category'] = '';
  $args['labels']['tax'] = '';
  $args['labels']['post_type'] = '';
  return $args;
}

//* Reposition the breadcrumbs
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );
add_action( 'genesis_before_content_sidebar_wrap', 'genesis_do_breadcrumbs' );

// Edit the read more link text
add_filter( 'excerpt_more', 'custom_read_more_link');
add_filter('get_the_content_more_link', 'custom_read_more_link');
add_filter('the_content_more_link', 'custom_read_more_link');
function custom_read_more_link() {
return '<a class="read-more-link" href="' . get_permalink() . '" rel="nofollow"></a>';
}

function opcstm_custom_excerpt_more( $output ) {
  if ( has_excerpt() && ! is_attachment() ) {
    $output .= custom_read_more_link();
  }
  return $output;
}
add_filter( 'get_the_excerpt', 'opcstm_custom_excerpt_more' );

include ('storm_reports_widget.php');
include ('new_horizons_widget.php');